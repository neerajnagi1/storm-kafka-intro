import java.util.List;
import java.util.ArrayList;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.utils.Utils;
import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;
import storm.kafka.HostPort;
import storm.kafka.KafkaConfig.StaticHosts;
import com.google.common.collect.ImmutableList;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class PyzeTopology 
{
    public static void main(String[] args) 
    {

//Kafka based spout
    	try{
    	Properties prop = new Properties();
    	InputStream input = new FileInputStream("config.properties");
		prop.load(input);
    	
		
	PyzeDynamoDbDaemon dynamoDbDaemon = new PyzeDynamoDbDaemon(Integer.parseInt(prop.getProperty("noOfDynamoWorker")));
	dynamoDbDaemon.init();
	List<HostPort> kafkaBrokers = new ArrayList<HostPort>();
	kafkaBrokers.add(new HostPort(prop.getProperty("kafkaBrokerHost"), Integer.parseInt(prop.getProperty("kafkaBrokerPort")) ));

	SpoutConfig spoutConfig = new SpoutConfig(new StaticHosts(kafkaBrokers,Integer.parseInt( prop.getProperty("noOfSpoutPartition") )),
//	SpoutConfig spoutConfig = new SpoutConfig(
//	ImmutableList.of("localhost:9092", "localhost:9092"), // list of Kafka brokers
//	8, // number of partitions per host
	  prop.getProperty("topicName"), // topic to read from
	  "/kafkastorm", // the root path in Zookeeper for the spout to store the consumer offsets
	  "discovery"); // an id for this consumer for storing the consumer offsets in Zookeeper
	KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);






        TopologyBuilder builder = new TopologyBuilder();

	builder.setSpout("kafka", kafkaSpout);
	builder.setBolt("second_bolt", new PyzeSecondBolt()).shuffleGrouping("kafka");
      builder.setBolt("minute_bolt", new PyzeMinuteBolt()).shuffleGrouping("second_bolt");
        builder.setBolt("hour_bolt", new PyzeHourBolt()).shuffleGrouping("minute_bolt");
        builder.setBolt("day_bolt", new PyzeDayBolt()).shuffleGrouping("hour_bolt");
        builder.setBolt("month_bolt", new PyzeMonthBolt()).shuffleGrouping("day_bolt");





        Config conf = new Config();
        
        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("test", conf, builder.createTopology());
        Utils.sleep(Integer.parseInt(prop.getProperty("topoloyLifeInSec")));
        cluster.killTopology("test");
        cluster.shutdown();
        
    	}
    	catch(Exception e){
    		System.out.println("EXCEPTION::PyzeTopology------------>"+e);
    	}
    }
}
