import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Arrays;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class PyzeSecondBolt extends BaseRichBolt 
{
    private OutputCollector collector;
    private PyzeRedis redis;
	private JSONParser parser ;
	private PyzeUtc pyzeUtc ;
	private String prefix ;
	private PyzeDynamoDb dynamoDb = null;
    public void prepare( Map conf, TopologyContext context, OutputCollector collector ) 
    {
        this.collector = collector;
	this.redis = new PyzeRedis();
	this.dynamoDb = new PyzeDynamoDb();
	this.parser = new JSONParser();
        this.pyzeUtc = new PyzeUtc();
        this.prefix = "s_";

    }

    public void execute( Tuple tuple ) 
    {
	try{
	byte[] bytes = tuple.getBinary(0);
	String msg = new String(bytes);
	System.out.println("::::::  Second Bolt  :::::::::::    \n" + msg);
	JSONObject msgObj = (JSONObject)parser.parse(msg);
	JSONObject mapObj = (JSONObject)msgObj.get("map");
	System.out.println(mapObj);
	Long bufferSize;
	String appKey = mapObj.get("pyzeAppKey").toString();
	String timestamp = mapObj.get("serverReceivedEpochTime").toString().substring(0,10);
	String appId = mapObj.get("pyzeAppInstanceId" ).toString();
	
	System.out.println("timeStamp:"+timestamp+ " appId:"+appId);
	 System.out.println("*******************************" +new java.util.Date()+"***********");
	if(( bufferSize = redis.addInCache(this.prefix + timestamp , appId  )) > 0  )
		{
		//TODO: handle dynamoDb failure case, currently forwarding tuple to next bolt even if it fails
			//dynamoDb.updateTable(this.prefix + appKey , timestamp , Long.toString(bufferSize));
			redis.addDynamoTask(this.prefix + appKey , timestamp , Long.toString(bufferSize));
			collector.emit(tuple, new Values(msg));
		}
		
        collector.ack( tuple );
	}
	catch(Exception e){
		System.out.println("Exception::"+e);
	}
    }

    public void declareOutputFields( OutputFieldsDeclarer declarer ) 
    {
        declarer.declare( new Fields( "msg" ) );
    }   
    
}
