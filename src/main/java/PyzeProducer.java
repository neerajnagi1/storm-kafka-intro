import java.util.*;
import java.util.UUID;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import kafka.javaapi.message.ByteBufferMessageSet;
import kafka.javaapi.producer.Producer;
import kafka.javaapi.producer.ProducerData;
import kafka.producer.ProducerConfig;
public class PyzeProducer {
    public static void main(String[] args) {
    	try{
        long events = Long.parseLong(args[0]);
        Random rnd = new Random();

        Properties props = new Properties();
    //	props.put("zk.connect", "127.0.0.1:2181/kafka");
        props.put("broker.list", "0:localhost:9092");
        props.put("serializer.class", "kafka.serializer.StringEncoder");
        props.put("request.required.acks", "1");

        ProducerConfig config = new ProducerConfig(props);

        Producer<String, String> producer = new Producer<String, String>(config);

        for (long nEvents = 0; nEvents < events; nEvents++) {
            long runtime = new Date().getTime();

		JSONObject _msg = new JSONObject();
		JSONObject _map = new JSONObject();
		_map.put("proximityState","far");
_map.put("currentWifiBSSID","");
_map.put("uptime","423793800");
_map.put("pyzeVersion","#todo");
_map.put("agentCollectionEpoch","1396297059560");
_map.put("alt","#todo");
_map.put("batteryState","Unknown");
_map.put("pyzeAppKey","com.pyze.AppLifecycle-AppStore.iOS");
_map.put("batteryLevel","unmonitored");
_map.put("lon","#todo");
_map.put("newView","TestViewController");
_map.put("userMem","846594048");
_map.put("topView","TestViewController");
_map.put("appReleaseVersion","25");
_map.put("appBuildVersion","3.5");
_map.put("address","#todo");
_map.put("osVersion","7.0");
if("random".equals(args[2]))
	_map.put("pyzeAppInstanceId",UUID.randomUUID().toString());
else
_map.put("pyzeAppInstanceId",args[2]);
_map.put("agentSentEpoch","1396297059000");
_map.put("lat","#todo");
_map.put("brightness","0.50");
_map.put("reachability","WiFi");
_map.put("newState","a");
_map.put("localDeviceTime","2014-03-31 13,17,39 -0700");
_map.put("orientation","Unknown");
_map.put("currentWifiSSID","");
if("system".equals(args[1]))
_map.put("serverReceivedEpochTime", String.valueOf(runtime));
else 
_map.put("serverReceivedEpochTime", args[1]);

		_msg.put("tableName" , "States");
		_msg.put("map" , _map);

		ProducerData<String, String> data = new ProducerData<String, String>("pyze_topic", JSONValue.toJSONString(_msg));
//            KeyedMessage<String, String> data = new KeyedMessage<String, String>("pyze_topic", ip, msg);
            producer.send(data);
       /*     try {
                Thread.sleep(10); 
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            */
        }
        producer.close();
    }
    	
        catch(Exception e){
        	System.out.println("usage is  PyzeProducer   no_of_message   epoch(system/fix)  appid(random/fix)");
        	
        }    	
    }

}
