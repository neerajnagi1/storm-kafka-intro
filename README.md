
Dependency installation
apt-get install git maven2 openjdk-7-jdk openjdk-7-jre redis-server



1) Kafka installation

```
cd /usr/src
wget http://archive.apache.org/dist/kafka/old_releases/kafka-0.7.2-incubating/kafka-0.7.2-incubating-src.tgz
tar zxvf kafka-0.7.2-incubating-src.tgz
cd kafka-0.7.2-incubating-src 
./sbt update
./sbt package
```

2) Zookeeper start-up
```
cd /usr/src/kafka-0.7.2-incubating-src/
nohup bin/zookeeper-server-start.sh config/zookeeper.properties > zookeeper.log &
```

3) Kafka server start-up
```
cd /usr/src/kafka-0.7.2-incubating-src/
nohup bin/kafka-server-start.sh config/server.properties > kafka_server.log &
```
4) redis start up 
```
/etc/init.d/redis-server start
```



Jar compilation

```
cd /usr/src
git clone  https://github.com/pyzedev/storm_pyze.git 
cd storm_pyze
mvn package
```

Storm installation

```
cd /usr/src
wget http://www.motorlogy.com/apache/incubator/storm/apache-storm-0.9.1-incubating/apache-storm-0.9.1-incubating.tar.gz
tar -zxvf apache-storm-0.9.1-incubating.tar.gz 
rm -rf /usr/src/apache-storm-0.9.1-incubating/lib/httpc*
cp /usr/src/storm_pyze/dependency/* /usr/src/apache-storm-0.9.1-incubating/lib/
add following line in ~/.bashrc file
	export PATH=$PATH:/usr/src/apache-storm-0.9.1-incubating/bin
source ~/.bashrc
```




Start topology( this test topology would just print recieved messges from kafka spout)
```
cd /usr/src/storm_pyze
storm jar target/pyze-1.0-SNAPSHOT-jar-with-dependencies.jar PyzeTopology test
```






Producer related Stuff

Send N number of message using kafka Producer. 2nd param is epoch time of message which could be either 
system : same a generation time
fix : some fixed time stamp like 1345567895

3rd param is for app id which could be either
random : new uuid for each msg
fix : some fixed value like askfja34jlk-jjfl34324-asjfljlkj-234jlfjlas
```
java -classpath target/pyze-1.0-SNAPSHOT-jar-with-dependencies.jar PyzeProducer N  system/fix  random/fix

```

Runtime Configuration

Different components could be configured with config.properties. Different fields are self explanatory.





