dependencies
    apt-get install ruby


load test 
	ruby loadtest.rb  num_of_producer  msg_per_producer    timestamp_type   app_id_type     

	num_of_producer : number of kafka producer e.g. 100

	msg_per_producer : number of  messages produced by each producer e.g. 1000

	timestamp_type : if  'system' then time of message creation will be used. Or it could be a fix value like '1370000000'		which  is epoch time in second. all message will have same timestamp then.

	app_id_type : if 'random' then a unique app id is used with each message. Or it could be a fix value like 'ajsldfjl-asdlfljl-ajdkfj' , all message will have same app id then.

Examples :  100X1000 messages with same timestamp of 1378900000 and unique app-id for each message.

	ruby loadtest.rb 100 1000 1378900000 random

